package com.aiss.transports.server;

import com.aiss.transports.server.resources.AuthResource;
import com.aiss.transports.server.resources.DriverResource;
import com.aiss.transports.server.resources.OrderResource;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class TransportApplication extends Application {
	private static Set<Object> singletons=new HashSet<Object>();
	private static Set<Class<?>> classes=new HashSet<Class<?>>();
	
	public TransportApplication(){
		singletons.add(OrderResource.getInstance());
		singletons.add(DriverResource.getInstance());
		singletons.add(AuthResource.getInstance());
	}
	
	@Override
	public Set<Class<?>> getClasses()
	{
		return classes;
	}
	
	@Override
	public Set<Object> getSingletons()
	{
		return singletons;
	}
	
	

}
