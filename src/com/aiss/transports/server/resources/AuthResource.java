package com.aiss.transports.server.resources;

import com.aiss.transports.server.repository.DAOTransportsRepository;
import com.aiss.transports.server.repository.TransportsRepository;

import org.jboss.resteasy.spi.UnauthorizedException;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/auth")
public class AuthResource {

	private static AuthResource _instance = null;

	private TransportsRepository repository;

	private AuthResource() {
		repository = DAOTransportsRepository.getInstance();
	}

	public static AuthResource getInstance() {
		if(_instance == null){
			_instance = new AuthResource();
		}
		return _instance;
	}

	@GET
	@Produces("text/plain")
	public String get(@HeaderParam("username") String user, @HeaderParam("password") String pass) {
		String res = repository.getAuthToken(user, pass);
		if(res==null){
			throw new UnauthorizedException("User or password incorrect");
		}
		return res;
	}
}
