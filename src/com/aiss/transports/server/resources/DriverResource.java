package com.aiss.transports.server.resources;

import com.aiss.transports.server.repository.DAOTransportsRepository;
import com.aiss.transports.server.repository.TransportsRepository;
import com.aiss.transports.shared.domain.Auth.Role;
import com.aiss.transports.shared.domain.Driver;
import com.aiss.transports.shared.domain.Order;

import org.jboss.resteasy.spi.NotFoundException;
import org.jboss.resteasy.spi.UnauthorizedException;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/drivers")
public class DriverResource {

	public static DriverResource _instance = null;

	private TransportsRepository repository;

	private DriverResource() {
//		repository = new MapTransportsRepository();
		repository = DAOTransportsRepository.getInstance();
	}

	public static DriverResource getInstance() {
		if(_instance == null){
			_instance = new DriverResource();
		}
		return _instance;
	}

	/**
	 * Returns all drivers
	 * 
	 * @return
	 */
	@GET
	@Produces("application/json")
	public Collection<Driver> getAllDrivers(@HeaderParam("authToken") String authToken) {
		Role role = repository.getAuthRole(authToken);
		if(role != Role.ADMIN && role != Role.DRIVER){
			throw new UnauthorizedException("Only drivers and administrator can acccess this information");
		}
		return repository.getAllDrivers();
	}

	/**
	 * Returns a specific driver
	 * 
	 * @return
	 */
	@GET
	@Path("/{driverID}")
	@Produces("application/json")
	public Driver getDriver(@PathParam("driverID") Long id, @HeaderParam("authToken") String authToken) {
		Role role = repository.getAuthRole(authToken);
		if(role != Role.ADMIN && role != Role.DRIVER){
			throw new UnauthorizedException("Only drivers and administrator can acccess this information");
		}
		Driver res = repository.getDriver(id);
		if(res==null){
			throw new NotFoundException("The driver with id "+id+" doesn't exists");
		}
		return res;
	}

	/**
	 * List orders assigned to a driver
	 * 
	 * @param driverID
	 * @return
	 */
	@GET
	@Path("/{driverID}/orders/")
	@Produces("application/json")
	public Collection<Order> getDriverAssignedOrders(@PathParam("driverID") Long driverID, @HeaderParam("authToken") String authToken) {
		Role role = repository.getAuthRole(authToken);
		if(role != Role.ADMIN && role != Role.DRIVER){
			throw new UnauthorizedException("Only drivers and administrator can acccess this information");
		}
		if(repository.getDriver(driverID)==null){
			throw new NotFoundException("The driver with id "+driverID+" doesn't exists");
		}
		return repository.getDriverAssignedOrders(driverID);
	}

}
