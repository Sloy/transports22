package com.aiss.transports.server.resources;

import com.aiss.transports.server.repository.DAOTransportsRepository;
import com.aiss.transports.server.repository.TransportsRepository;
import com.aiss.transports.shared.domain.Auth.Role;
import com.aiss.transports.shared.domain.Order;

import org.jboss.resteasy.spi.BadRequestException;
import org.jboss.resteasy.spi.NotFoundException;
import org.jboss.resteasy.spi.UnauthorizedException;

import java.net.URI;
import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

@Path("/orders")
public class OrderResource {
	private static OrderResource _instance = null;

	private TransportsRepository repository;

	private OrderResource() {
//		repository = new MapTransportsRepository();
		repository = DAOTransportsRepository.getInstance();
	}

	public static OrderResource getInstance() {
		if(_instance == null){
			_instance = new OrderResource();
		}
		return _instance;
	}

	/* ---- GETs ---- */

	/**
	 * Returns all orders as a json list
	 */
	@GET
	@Produces("application/json")
	public Collection<Order> getAll() {
		return repository.getAllOrders();
	}

	/**
	 * Returns the specified order
	 */
	@GET
	@Path("/{orderID}")
	@Produces("application/json")
	public Order get(@PathParam("orderID") Long id) {
		Order res = repository.getOrder(id);
		if(res==null){
			throw new NotFoundException("The order with refNumber " + id + " was not found");
		}
		return res;
	}

	/**
	 * Creates a new order
	 * 
	 * @param uriInfo
	 * @param o 
	 * @return
	 */
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response addOrder(@Context UriInfo uriInfo, Order o) {
		// Validates the order
		if(o.getDestinationAddress() == null || o.getSourceAddress() == null){
			throw new BadRequestException("Addresses can't be empty");
		}
		if(o.getRefNumber() != 0){
			throw new BadRequestException("New orders must arrive with refNumber 0, and a new one will be assigned");
		}
		if(o.getDriverID() != 0){
			throw new BadRequestException("New orders must arrive with driverID 0, and a new one will be assigned later by the administrator");
		}
		if(o.getStatus()!=Order.Status.PENDING){
			throw new BadRequestException("New orders must arrive with status PENDING or 0");
		}

		// Saves the order
		o = repository.putOrder(o);

		// Builds the response
		UriBuilder ub = uriInfo.getAbsolutePathBuilder().path(this.getClass(), "get");
		URI uri = ub.build(o.getRefNumber());
		ResponseBuilder resp = Response.created(uri);
		resp.entity(o);
		return resp.build();
	}

	/**
	 * Update the order. Assigning a driver, or updating the status
	 * 
	 * @param id
	 * @param o
	 * @param authToken Authorization token required for the operation
	 * @return 204 if ok, 401 if not enough privileges
	 */
	@PUT
	@Path("/{orderID}")
	@Consumes("application/json")
	public Response updateOrder(@PathParam("orderID") Long id, Order o, @HeaderParam("authToken") String authToken) {
		Order old = repository.getOrder(id);
		// Gets the authorization
		Role role = repository.getAuthRole(authToken);
		// If no authorization, error
		if(role == Role.UNAUTHORIZED){
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		// Checks that order exists
		if(old == null){
			throw new NotFoundException("The order with refNumber " + id + " was not found");
		}
		// And all de non-mutable fields remains the same
		if(!(o.getDestinationAddress().equals(old.getDestinationAddress()) && o.getSourceAddress().equals(old.getSourceAddress())
				&& o.getProductType().equals(old.getProductType()) && o.getProductQuantity() == old.getProductQuantity())){
			return Response.status(javax.ws.rs.core.Response.Status.CONFLICT).build();
		}
		// The edited field is allowed for the role
		if(o.getDriverID() != old.getDriverID() && role != Role.ADMIN){// driver
																		// changed
			throw new UnauthorizedException("Only the admin can assign a different driver to a order");
		}
		if(o.getStatus() != old.getStatus() && role != Role.DRIVER){ // status
																		// changed
			// TODO puede el admin cambiarlo tambi�n?
			throw new UnauthorizedException("Only the driver can change the order status");
		}
		// Update the repository
		repository.putOrder(o);
		// Send the response
		return Response.noContent().build();
	}
}
