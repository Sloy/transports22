package com.aiss.transports.server.repository;

import com.aiss.transports.shared.domain.Auth;
import com.aiss.transports.shared.domain.Auth.Role;
import com.aiss.transports.shared.domain.Driver;
import com.aiss.transports.shared.domain.Order;
import com.aiss.transports.shared.domain.Order.Status;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapTransportsRepository implements TransportsRepository {

	Map<Long, Order> ordersMap;
	Map<Long, Driver> driversMap;
	Map<String, Auth.Role> roleToken = new HashMap<String, Role>();

	public MapTransportsRepository() {
		ordersMap = new HashMap<Long, Order>();
		driversMap = new HashMap<Long, Driver>();
		initialize();
	}

	/**
	 * Test Data
	 */
	private void initialize() {
		roleToken.put("2321ASD2223093PPP33939JX847433K", Role.ADMIN);
		roleToken.put("PPRUD8844200LK3J4JGB22NI444JN1R", Role.DRIVER);

		Driver d = new Driver();
		d.setName("Rafa");
		d.setSurname("Baskes");
		d.setId(1L);
		driversMap.put(d.getId(), d);

		Order o1 = new Order();
		o1.setRefNumber(1L);
		o1.setStatus(Status.PENDING);
		o1.setProductQuantity(2);
		o1.setProductType("drink");
		o1.setSourceAddress("Avenue 3");
		o1.setDestinationAddress("Avenuelta 43");
		o1.setDriverID(d.getId());

		Order o2 = new Order();
		o2.setRefNumber(2L);
		o2.setStatus(Status.PENDING);
		o2.setProductQuantity(2);
		o2.setProductType("fruit");
		o2.setSourceAddress("Alham 5");
		o2.setDestinationAddress("Remhun 783");
		o2.setDriverID(d.getId());

		putOrder(o1);
		putOrder(o2);
	}

	/* --- Orders --- */

	@Override
	public Collection<Order> getAllOrders() {
		return ordersMap.values();
	}

	@Override
	public Order putOrder(Order o) {
		if(o.getRefNumber() == 0){
			// Creates a new ref number
			o.setRefNumber((long)ordersMap.size());
		}
		ordersMap.put(o.getRefNumber(), o);
		return o;

	}

	@Override
	public Order getOrder(Long name) {
		return ordersMap.get(name);
	}

	/* --- Drivers --- */

	@Override
	public Collection<Order> getDriverAssignedOrders(Long driverID) {
		List<Order> res = new ArrayList<Order>();
		for(Order o : ordersMap.values()){
			if(o.getDriverID() == driverID){
				res.add(o);
			}
		}
		return res;
	}

	@Override
	public Driver getDriver(Long driverID) {
		return driversMap.get(driverID);
	}

	@Override
	public Collection<Driver> getAllDrivers() {
		return driversMap.values();
	}

	/* -- Auth -- */
	@Override
	public Role getAuthRole(String token) {
		Role res = roleToken.get(token);
		if(res == null){
			res = Role.UNAUTHORIZED;
		}
		return res;
	}

	@Override
	public String getAuthToken(String user, String pass) {
		//toy example:
		if(user==null || pass==null){
			return null;
		}
		if(user.equals("admin") && pass.equals("1234")){
			return "2321ASD2223093PPP33939JX847433K";
		}
		if(pass.equals("imadriver")){
			return "PPRUD8844200LK3J4JGB22NI444JN1R";
		}
		return null;
	}

}
