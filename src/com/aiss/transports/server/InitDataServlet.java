package com.aiss.transports.server;

import com.aiss.transports.server.repository.DAOTransportsRepository;
import com.aiss.transports.shared.domain.Driver;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InitDataServlet extends HttpServlet {

	private static final long serialVersionUID = -6452399276475986405L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Driver r = new Driver("Rafa", "Baskes");
		Driver f = new Driver("Fran", "Garmo");
		Driver a = new Driver("Albertitou", "Rinco");
		
		DAOTransportsRepository.getInstance(); //initialize
		Objectify ofy = ObjectifyService.begin();
		//TODO borrar todo el contenido actual
		
		ofy.put(r,f,a); //simple add
		
		assert r.getId() != null && f.getId() != null && a.getId() != null;
		resp.getWriter().write("Done");
		resp.setStatus(201);
	}

	
}
