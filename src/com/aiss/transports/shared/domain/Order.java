package com.aiss.transports.shared.domain;

import javax.persistence.Id;

public class Order {

	public enum Status {
		PENDING, SHIPPED, SERVED
	}

	private @Id
	Long refNumber;
	private String destinationAddress;
	private String sourceAddress;
	private String productType;
	private Integer productQuantity;
	private Status status;
	private Long driverID;

	public Order() {
	}

	public Order(String destinationAddress, String sourceAddress, String productType, Integer productQuantity) {
		super();
		this.destinationAddress = destinationAddress;
		this.sourceAddress = sourceAddress;
		this.productType = productType;
		this.productQuantity = productQuantity;
		this.status = Status.PENDING;
		this.refNumber = null;
		this.driverID = 0L;
	}

	public String getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public String getSourceAddress() {
		return sourceAddress;
	}

	public void setSourceAddress(String sourceAddress) {
		this.sourceAddress = sourceAddress;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public int getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public long getDriverID() {
		return driverID;
	}

	public void setDriverID(long driverID) {
		this.driverID = driverID;
	}

	public Long getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(Long id) {
		this.refNumber = id;
	}

}
